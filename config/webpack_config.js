/**
 * webpack打包的基本加载机
 * @vue-loder 处理.vue文件的loader
 * @babel-loader 高版本的js转化低版本浏览器能识别的js
 * @url-loader 处理资源文件，处理路径上的对应关系
 * @css-loader 编译css文件的loader
 * @file-loader 处理资源文件，文件本省打拷贝等操作
 * @json-loader 处理json文件
*/

//引入path模块，处理文件间的跳转
const path = require('path')
const webpack = require('webpack')

//引入loader文件中的配置文件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const htmlWebpackPlugin = require('html-webpack-plugin')

//定义当前路径
const projectPublic = path.resolve(__dirname,'../')

//定义编译文件的wabpack
const webpackConfig = {
    mode:'development',
    //首先定义入口文件
    entry: {
        app: path.join(projectPublic,'./src/index.js')
    },
    //定义出口文件
    output: {
        path: path.join(projectPublic,'./dist/'),
        //定义编译打包后的文件名 
        filename:'static/js/[name].js',
        //打包后的文件自动会注入到index.html文件中，定义src的输入名称
        publicPath: './'
    },
    //设置打包过程中需要编译的文件
    resolve: {
        extensions: ['.js','.vue','.css','html']
    },
    //module是打包过程中的中间模块，处理各个文件
    module: {
        rules:[
            {
                test:/\.js$/,
                loader:'bable-loader'
            },
            {
                test:/\.vue$/,
                loader:'vue-loader',
            },
            {
                test:/\.css$/,
                loader:'vue-style-loader','css-loader'
            },
            {
                test:/\.json$/,
                loader:'json-loader'
            },
            {
                test:/\.(png|jpe?g|gif|)/,
                loader:'url-loader',
                query:{
                    limit:10000,
                    name:'static/img/[name]_[hash:7]'
                }
            }
        ]
    },
    Plugin: [
        new VueLoaderPlugin(),
        new htmlWebpackPlugin({
            filename: 'index.html',
            template:path.join(projectPublic,'./src/index.html'),
            inject: true
        })
    ]
}

//向外暴露模块
module.exports = webpackConfig