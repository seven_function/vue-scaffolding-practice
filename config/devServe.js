//引入express 插件
let express = require('express')
//引入webapck
let webpack = require('webpack')
//引入写好的配置
let webpackConfig = require('./webpack_config')
//引入中间件
let devMiddleware = require('webpack-dev-middleware')
//定义端口号
let PORT = 3081
//开启一个服务器
let app = express()

//使用webpack配置，使单vue文件能被webpack进行编译
let compailer = webpack(webpackConfig)
//使用编译器创建一个express中间件
let middleware = devMiddleware(compailer, {
    publicPath: "/",
    index: 'index.html'
})

//将中间件纳入exPress创建的服务器中
app.use(middleware)

//防止网页找不到facicon.ico文件报错进行出路
app.use('facicon.ico',(res,err) => {
    res.end("")
})

//启动服务端口，进行监听
app.listen(PORT,err => {
    if (err) {
        console.log("启动程序报错————————————————————",err)
    } else {
        console.log("程序正常启动监听在"+ PORT+'端口')
    }

})